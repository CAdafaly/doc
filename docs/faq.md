---
author: Pierre Smeyers
---

# Frequently Asked Questions

## What is the license?

_to be continuous_ is an Open Source project licensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html),
version 3.0.

You can **use it for commercial purposes**, you can **(re)distribute it**, you can **modify it** under certain conditions.
More info available on [choosealicense.com](https://choosealicense.com/licenses/gpl-3.0/).

## Who develops _to be continuous_?

This has been an internal project at [Orange](https://orange.com) for 2 years before becoming an Open Source project.

The project is still mainly developed and maintained by Orange developers, but anyone is welcome to [contribute](../dev/workflow/).

## Can I use _to be continuous_ on my self-managed GitLab?

(a.k.a. _"on-premise" GitLab_)

Yes.

We have an extensive documentation about [how to use _to be continuous_ in your own self-managed GitLab](../self-managed/basic/).
