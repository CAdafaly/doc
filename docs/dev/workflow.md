---
author: Pierre Smeyers
---

# Git workflow & review process

This page presents the global workflow for contributing to _to be continuous_ development.

## Legal

All original contributions to _to be continuous_ are licensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html),
version 3.0 or later.

All contributions are subject to the [Developer Certificate of Origin](https://developercertificate.org/) (DCO).
The DCO is a lightweight way for contributors to certify that they wrote or otherwise have the right to submit the code they are contributing to the project.

Contributors **must** _sign-off_ that they adhere to these requirements by adding a `Signed-off-by` line to commit messages, as shown below:

```text
This is the commit message

Signed-off-by: John Dev <john.dev@developer.example.org>
```

Git has a handy [`-s` command line option](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff) to append this automatically to your commit message:

```bash
$ git commit -s -m 'This is the commit message'
```

## Contribute to an existing template

!!! IMPORTANT
    All submissions, including submissions by project members, need to be reviewed before being merged.

To contribute:

1. Create an issue describing the bug or enhancement you want to propose (select the right issue template).
2. Make sure the issue has been reviewed and agreed.
3. Create a Merge Request, from your **own** fork (see [forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) documentation).
   Don't hesitate to mark your MR as `Draft` as long as you think it's not ready to be reviewed.
4. When merged, a new [release will be performed](#release-process).

## Initial release of a new template (advanced)

1. Start posting a message with topic `New template: bla bla` on the [to be continuous Discord channel](https://discord.gg/SetvpZ9XZ6),
and present the scope and goal of your template.
2. If agreed by the core team, a new project will be created from [template skeleton](https://gitlab.com/to-be-continuous/tools/template-skeleton)
   with private visibility in the [to be continuous GitLab group](https://gitlab.com/to-be-continuous). You'll be granted `developer` access to it.
3. Fork the project to your own space, select the right `initial-xxx` branch to start from (depending on the kind of template you're developing).
4. As soon as you like, create a Merge Request to the original repository in _Draft_ mode.
5. You may ask for help, advices and reviews anytime during your development process.
6. Once you think your template is ready, mark it as _Ready_ and ask for a review.
7. An initial development shall be reviewed and approved by at least 2 other contributors from the core team.
8. When approvals are met: the MR can be merged, and an initial [release will be performed](#release-process).
9. Then the project visibility is changed to public, and your template becomes official :tada:.

## Release process

The release process is based on [semantic-release](https://semantic-release.gitbook.io/).
As a result, releases are performed automatically when commits on the `master` branch indicate a new release is required (see below).

## Git Commit Guidelines

Commits in your merge requests should be **atomic** and **semantic**.

### Atomic

* Please properly squash your merge requests before submitting them.
* Fixup commits can be used temporarily during the review process but things should be squashed at the end to have meaningful commits. 
* If you need help to do that, just ask :)

### Semantic

We are using [semantic-release](https://semantic-release.gitbook.io/) to manage our template releases.

This nice tool automatically takes care or version management, package publishing and release note generation,
based on [Commit message syntax](https://semantic-release.gitbook.io/semantic-release/#commit-message-format).

Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```text
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier
to read on GitLab as well as in various git tools.

#### Revert

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header
of the reverted commit.
In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit
being reverted.

#### Type

Must be one of the following:

| Type              | description                                  | Release type |
| ----------------- | -------------------------------------------- | ------------ |
| `feat`            | A new feature                                | **minor**    |
| `fix`             | A bug fix                                    | **patch**    |
| `docs`            | Documentation only change(s)                 | _none_       |
| `style`           | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) | _none_       |
| `refactor`        | A code change that neither fixes a bug nor adds a feature | _none_       |
| `perf`            | A code change that improves performance      | _none_       |
| `test`            | Adding missing or correcting existing tests  | _none_       |
| `chore`           | Changes to the build process or auxiliary tools such as doc. generation, CI/CD| _none_       |

:warning: **breaking changes** shall be mentionned in the [footer](#footer) by prefixing line(s) with `BREAKING CHANGE:`.

#### Scope

The scope is optional and it could be anything specifying the place of the commit change. For example `CI/CD`,
`sonarqube`, `deploy`, `report` ...

#### Subject

The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

#### Body

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

#### Footer

The footer should contain any information about **Breaking Changes** and is also the place to
[reference GitLab issues that this commit closes](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).

:warning: **breaking changes** should start with the word `BREAKING CHANGE:` with a space or two newlines.
The rest of the commit message is then used for this.
