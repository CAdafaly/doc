---
author: Pierre Smeyers
description: This page presents the general principles of use supported throughout all to-be-continuous templates.
---

# Using _To Be Continuous_

This page presents the general principles of use supported throughout all _to be continuous_ templates.

## Include a template

As previously said, each template may be used by [including](https://docs.gitlab.com/ee/ci/yaml/#includefile) it to your
`.gitlab-ci.yml` file.

For example:

```yaml
include:
  - project: 'to-be-continuous/maven' # this is the template project
    file: '/templates/gitlab-ci-maven.yml' # template file within the project
    ref: '1.0.0' # template exact version
  - project: 'to-be-continuous/aws'
    file: '/templates/gitlab-ci-aws.yml'
    ref: '1.2' # use minor version alias (uses the latest available patch version)
```

Our templates are **versioned** (compliant with [Semantic Versioning](https://semver.org/)):

* each version is exposed through a Git tag such as `1.1.0`, `2.1.4`, ...
* for convenience purpose, our templates also maintain a **minor version alias** tag (ex: `2.1`), always referencing the latest patched version within that minor version, 
  and also a **major version alias** tag (ex: `2`), always referencing the latest minor version within that major version.
* our recommendation is to **use a fixed version** of each template (either exact, minor or major), and upgrade when a new valuable feature is rolled out.
* you may also chose to use the **latest released** version (discouraged as a new version with breaking changes would break your pipeline).
  For this, simply include the template from `master`.

## Configure a template

Each template comes with a predefined configuration (whenever possible), but is always overridable via [variables](https://docs.gitlab.com/ee/ci/variables/).

Some template features are also enabled by defining the right variable(s).

Here is an example of a Maven project that:

1. overrides the Maven version used (with `MAVEN_IMAGE` variable),
2. overrides the build arguments (with `MAVEN_BUILD_ARGS`),
3. enables [SonarQube](https://www.sonarqube.org/) analysis (by defining `SONAR_URL` and `SONAR_AUTH_TOKEN`),

```yaml
include:
  - project: 'to-be-continuous/maven'
    file: '/templates/gitlab-ci-maven.yml'
    ref: '1.0.0'

variables:
  # use Maven 3.6 with JDK 8
  MAVEN_IMAGE: "maven:3.6-jdk-8"
  # use 'cicd' Maven profile
  MAVEN_BUILD_ARGS: 'verify -Pcicd'
  # enable SonarQube analysis
  SONAR_URL: "https://mysonar.domain.my"
  # SONAR_AUTH_TOKEN defined as a secret CI/CD variable
```

This is the basic pattern for configuring the templates!

You'll find configuration details in each template reference documentation.

## Debugging _to be continuous_ jobs

Each template enable debug logs when `$TRACE` is set to `true`.

So you may simply manually run your pipeline, and set `TRACE=true` interactively.

:warning: this is different (and complementary) to GitLab's [`CI_DEBUG_TRACE`](https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging) variable.

## Docker Images Versions

_to be continuous_ templates use - whenever possible - required tools as Docker images.
And when available, the _latest_ image version is used.

In some cases, using the latest version is a good thing, and in some other cases, the latest version is bad.

* _latest_ is **good** for:
    * DevSecOps tools (Code Quality, Security Analysis, Dependency Check, Linters ...) as using the latest version of the tool is the best way to ensure
      you're likely to detect vulnerabilities as soon as possible (well, as soon as new vulnerabilities are known and covered by DevSecOps tools).
    * Public cloud CLI clients as there is only one version of the public cloud, and the official Docker image is likely to evolve at the same time as the APIs.
* _latest_ is **not good** for:
    * Build tools as your project is developped using one specific version of the language / the build tool, and you would like to control when you change version.
    * Infrastructure-as-Code tools for the same reason as above.
    * Acceptance tests tools as the same reason as build tools.
    * Private cloud CLI clients as you may not have installed the latest version of - say - OpenShift or Kubernetes, and you'll need to use the client CLI version that matches your servers version.

!!! Success "To summarize"

    1. Make sure you explicitely override the Docker image versions of your build, Infrastructure-as-Code, private cloud CLI clients and acceptance tests tools matching your project requirements.
    2. Be aware that sometimes your pipeline may fail (without any change from you) due to a new version of DevSecOps tool that either highlights a new vulnerability (:tada:), or due to a bug or breaking change in the tool (:poop: happens).

## Secrets managements

Most of our templates manage :lock: **secrets** (access tokens, user/passwords, ...).

Our general recommendation for those secrets is to [manage them as project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui):

* [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-custom-variable) to prevent them from being inadvertently
  displayed in your job logs,
* [**protected**](https://docs.gitlab.com/ee/ci/variables/#protect-a-custom-variable) if you want to secure some secrets
  you don't want everyone in the project to have access to (for instance production secrets).

### What if a secret can't be masked?

It may happen that a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#masked-variable-requirements).

In that case there is a simple solution: simply encode it in [Base64](https://en.wikipedia.org/wiki/Base64) and declare
the variable value as the Base64 string prefixed with `@b64@`. This value **can** be masked, and it will be automatically
decoded by our templates (make sure you're using a version of the template that supports this syntax).

!!! EXAMPLE
    `CAVE_PASSPHRASE={"open":"$€5@me"}` can't be masked, but the Base64 encoded secret can.
     
    Then just declare instead:
    
    `CAVE_PASSPHRASE=@b64@eyJvcGVuIjoiJOKCrDVAbWUifQ==`

## Scoped variables

All our templates support a generic and powerful way of limiting/overriding some of your environment variables, depending on the execution context.

This feature is comparable to GitLab [Scoping environments with specs](https://docs.gitlab.com/ee/ci/environments/index.html#scope-environments-with-specs)
feature, but covers a broader usage:

* can be used with **non-secret variables** (defined in your `.gitlab-ci.yml` file),
* variables can be scoped by **any other criteria than deployment environment**.

The feature is based on a specific variable naming syntax:

```bash
# syntax 1: using a unary test operator
scoped__<target var>__<condition>__<cond var>__<unary op>=<target val>

# syntax 2: using a comparison operator
scoped__<target var>__<condition>__<cond var>__<cmp op>__<cmp val>=<target val>
```

:warning: mind the **double underscore** that separates each part.

Where:

| Name           | Description                | Possible values / examples |
| -------------- | -------------------------- | -------------------------  |
| `<target var>` | Scoped variable name       | any <br/> example: `MY_SECRET`, `MAVEN_BUILD_ARGS`, ... |
| `<condition>`  | The test condition         | one of: `if` or `ifnot`    |
| `<cond var>`   | The variable on which relies the condition | any <br/> example: `CI_ENVIRONMENT_NAME`, `CI_COMMIT_REF_NAME`, ... |
| `<unary op>`   | Unary test operator to use | only: `defined` |
| `<cmp op>`     | Comparison operator to use | one of: `equals`, `startswith`, `endswith`, `contains`, `in`<br/>or their _ignore case_ version: `equals_ic`, `startswith_ic`, `endswith_ic`,`contains_ic` or `in_ic` |
| `<cmp val>`    | Sluggified value to compare `<cond var>` against | any<br/> With `in` or `in_ic` operators, matching values shall be separated with **double underscores** |
| `<target val>` | The value `<target var>` takes when condition matches | any (can even use other variables that will be expanded) |

!!! QUESTION "Which variables support this?"
    The scoped variables feature has a **strong limitation**: it may only be used for variables used in the `script` and/or `before_script` parts; not elsewhere in the `.gitlab-ci.yml` file.
    
    :red_circle: They **don't support _scoped variables_**:

    * variables used to parameterize the jobs Docker image(s) (ex: `MAVEN_IMAGE` or `K8S_KUBECTL_IMAGE`),
    * variables that enable/disable some jobs behavior (ex: `MAVEN_DEPLOY_ENABLED`, `NODE_AUDIT_DISABLED` or `AUTODEPLOY_TO_PROD`).

    :white_check_mark: They **do support _scoped variables_**:

    * credentials (logins, passwords, tokens, ...),
    * configuration URLs,
    * tool CLI options and arguments (ex: `MAVEN_BUILD_ARGS` or `PHP_CODESNIFFER_ARGS`)

    _If you have any doubt: have a look at the template implementation._

!!! QUESTION "How variable values are sluggified?"
    Each character that is not a **letter**, a **digit** or **underscore** is replaced by an underscore (`_`).

    Examples:

    * `Wh@t*tH€!h3¢k` becomes: `Wh_t_tH__h3_k`
    * `feat/add-welcome-page` becomes: `feat_add_welcome_page`

!!! EXAMPLE "Example 1: scope by environment"
    ```yaml
    variables:
      # default configuration
      K8S_URL: "https://my-nonprod-k8s.domain"
      MY_DATABASE_PASSWORD: "admin"

      # overridden for prodution environment
      scoped__K8S_URL__if__CI_ENVIRONMENT_NAME__equals__production: "https://my-prod-k8s.domain"
      # MY_DATABASE_PASSWORD is overridden for prod in my project CI/CD variables using
      # scoped__MY_DATABASE_PASSWORD__if__CI_ENVIRONMENT_NAME__equals__production
    ```

!!! EXAMPLE "Example 2: scope by branch"
    ```yaml
    variables:
      # default Angular build arguments (default configuration)
      NG_BUILD_ARGS: "build"

      # use 'staging' configuration on develop branch
      scoped__NG_BUILD_ARGS__if__CI_COMMIT_REF_NAME__equals__develop: "build --configuration=staging"

      # use 'production' configuration and optimization on master branch
      scoped__NG_BUILD_ARGS__if__CI_COMMIT_REF_NAME__equals__master: "build --configuration=production --optimization=true"
    ```

!!! EXAMPLE "Example 3: scope on tag"
    ```yaml
    variables:
      # default Docker build configuration
      DOCKER_BUILD_ARGS: "--build-arg IMAGE_TYPE=snapshot"

      # overridden when building image on tag (release)
      scoped__DOCKER_BUILD_ARGS__if__CI_COMMIT_TAG__defined: "--build-arg IMAGE_TYPE=release"
    ```

## Proxy configuration

Our templates don't have any proxy configuration set by default, but they all support standard Linux variables:

* `http_proxy`
* `https_proxy`
* `ftp_proxy`
* `no_proxy`

As a result, you may perfectly define those variables in your project:

* either globally as group or project variables or in the top variables block definition of your `.gitlab-ci.yml` file,
* either locally in specific jobs,
* or for all jobs from one single template ([see below](#the-templates-base-job)).

## Certificate Authority configuration

Our templates all come configured with the Default Trusted Certificate Authorities, but they all support the `CUSTOM_CA_CERTS`
variable to configure additional certificate authorities.

When set, this variable shall contain one or several certificates in [PEM format](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail),
then the template will assume those are trusted certificates, and add them accordingly to the right trust store.

Again, you may perfectly set `CUSTOM_CA_CERTS` in your project:

* either globally as group or project variables or in the top variables block definition of your `.gitlab-ci.yml` file,
* either locally in specific jobs,
* or for all jobs from one single template ([see below](#the-templates-base-job)).

## Merge Request workflow

One thing that has to be chosen with GitLab CI/CD is the [Merge Request workflow strategy](https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines).

By default, _to be continuous_ implements the **branch pipelines** strategy, with the following workflow declaration:

```yaml
workflow:
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always
```

If you want to switch to the **merge request pipelines** strategy, simply add the following to your `.gitlab-ci.yml` file:

```yaml
# use Merge Request pipelines rather than branch pipelines
workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always
```

## Advanced usage - Override YAML

Sometimes, configuration via variables is not enough to tweak an existing template to fit to your needs.

Fortunately, GitLab CI [include](https://docs.gitlab.com/ee/ci/yaml/#includefile) feature is implemented in a way that
allows you to **override the included YAML code**.

!!! QUOTE "from [GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/#include)"

    The files defined in include are:
    
    * Deep merged with those in `.gitlab-ci.yml`.
    * Always evaluated first and merged with the content of `.gitlab-ci.yml`, regardless of the position of the include keyword.

In order to override the included templates YAML code, you'll probably have to deep dive into it and understand how it
is designed.

### The templates base job

A very important thing you should be aware of is that every template defines a ([hidden](https://docs.gitlab.com/ee/ci/yaml/README.html#hide-jobs))
base job, [extended](https://docs.gitlab.com/ee/ci/yaml/#extends) by all other jobs.
That might not be the case for templates that declare **one single job**.

For example the Maven template defines the `.mvn-base` base job.

Thus, if you wish to override something **for all the jobs from a specific template**, this is the right place to do
the magic.

### Example 1: add Docker services

In this example, let's consider my Java project needs a MySQL database to run its unit tests.

According to the Maven template implementation, that can be done by overriding the `mvn-build` job as follows:

```yaml
mvn-build:
  services:
    - name: mysql:latest
      alias: mysql_host
  variables:
    MYSQL_DATABASE: "acme"
    MYSQL_ROOT_PASSWORD: "root"
```

Those changes will gracefully be merged with the `mvn-build` job, the rest of it (defined by the Maven template) will
remain unchanged.

### Example 2: run on private runners with proxy

In this example, let's consider my project needs to deploy on a Kubernetes cluster that is only
accessible from my [private runner](https://docs.gitlab.com/ee/ci/runners/#specific-runner) (with tags
`kubernetes`, `private`), and that requires an http proxy.

According to the Kubernetes template implementation, that can be done by overriding the base `.k8s-base` job as follows:

```yaml
.k8s-base:
  # set my runner tags
  tags:
    - kubernetes
    - private
  # set my proxy configuration
  variables:
    http_proxy: "http://my.proxy:8080"
    https_proxy: "http://my.proxy:8080"
```

This way, all Kubernetes jobs will inherit this configuration.
