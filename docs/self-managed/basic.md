---
author: Pierre Smeyers
---

# self-managed _to be continuous_ (basic)

This page details how to install and use _to be continuous_ (**tbc**) in a self-managed GitLab.

## Copy tbc to your GitLab

The first thing to do is to copy the complete [tbc group](https://gitlab.com/to-be-continuous/) structure to your own GitLab.

Don't panic, we provide all required tools to initiate it for the first time, and also schedule regular updates to keep synchronized.

1. Create an empty `to-be-continuous` root group with `public` visibility.
2. Create a dedicated _non-individual_ GitLab account, add it to the `to-be-continuous` root group
   with `Maintainer` role.
3. Generate a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with scopes `api,read_registry,write_registry,read_repository,write_repository`.
4. Execute the following command to **recursively copy the tbc group**:
    ```bash
    curl -s https://gitlab.com/to-be-continuous/tools/gitlab-sync/-/raw/master/gitlab-sync.sh | bash /dev/stdin --dest-api https://your.gitlab.host/api/v4 --dest-token $GITLAB_TOKEN --exclude samples,custom
    ```

That should take a while, but hopefully at the end you'll have cloned the complete _to be continuous_
group and projects :tada:.

## Build the tracking image

Every _to be continuous_ template uses the [tracking](https://gitlab.com/to-be-continuous/tools/tracking) Docker image as a [service container](https://docs.gitlab.com/ee/ci/services/).

As a result you won't be able to run any pipeline as long as don't have any tracking image available in the registry.

Go in your local copy of the [tools/tracking](https://gitlab.com/to-be-continuous/tools/tracking) project, and manually click
the last `docker-publish` job that should be pending (if it's not the case, manually start a fresh new pipeline, then click the `docker-publish` job when it's pending). It may happen that the publish job fails the first time: simply retry it.

We'll see later [how to configure the tracking project](../advanced/#setup-tracking) to collect statistics about template jobs execution.
For now it is unconfigured, and will not track anything.

## Sync. your local copy of tbc

From this point, you can start using _to be continuous_ templates from your GitLab.
But it is highly probable that you'll want to get automatically latest updates / new templates from the
original _to be continuous_ project.

For this, you only have to create a [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) in your local copy of the [tools/gitlab-sync](https://gitlab.com/to-be-continuous/tools/gitlab-sync) project:

1. declare the CI/CD project variable `GITLAB_TOKEN` with the previously created token (mark it as [masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-custom-variable)),
2. create a scheduled pipeline (for instance every day at 2:00 am).

All other required variables shall be retrieved from [GitLab CI predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

!!! IMPORTANT
    From this point, you might not make any commit in any local copy of _to be continuous_ projects
    because it will get overwritten every night.

    If you need to modify template code, you'll have 2 options depending on your case:

    * if it's a general enhancement/fix: make a contribution to the Open Source [to be continuous project](https://gitlab.com/to-be-continuous/) and get the change through the synchronization task,
    * if it's a change specific to your company: see [advanced usage](../advanced/)

By the way, you can manually trigger a pipeline in your [tools/gitlab-sync](https://gitlab.com/to-be-continuous/tools/gitlab-sync) project
anytime to synchronize your _to be continuous_ copy.

## Add your custom CA certificates

If your company is using non-Default Trusted Certificate Authorities to generate server certificates, it is highly probable that some _to be continuous_ template jobs will fail because such or such tool failed validating SSL certificates.

As explained in the [usage guide](../../usage/#certificate-authority-configuration), each project might
define a `CUSTOM_CA_CERTS` variable either as a group/project variable, or in its `.gitlab-ci.yml` file to declare the custom company Certificate Authorities.

But there is also a global way to fix this. Ask your GitLab administrator to declare `DEFAULT_CA_CERTS`
as an [instance CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#instance-cicd-variables).
Similar to `CUSTOM_CA_CERTS`, `DEFAULT_CA_CERTS` shall contain one or several certificates in [PEM format](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail).

Every _to be continuous_ template job - prior to executing - determines whether a `$CUSTOM_CA_CERTS` or else `$DEFAULT_CA_CERTS` is defined and adds its content to the trusted CA certificates.
