---
author: Pierre Smeyers
description: This page introduces general notions & philosophy about to-be-continuous.
---

# Understand _To Be Continuous_

This page introduces general notions & philosophy about _to be continuous_.

## A state-of-the art pipeline?!

Generally speaking, a CI/CD pipeline should be composed of one or several of the following stages:

1. **compile** the code and **package** it into an executable or intermediate format
2. perform all required **tests** and code **analysis**:
    * unit testing
    * code quality audits
    * Static Application Security Testing ([SAST](https://en.wikipedia.org/wiki/Static_program_analysis))
    * dependencies check
    * licenses check
    * ...
3. **package** the compiled code into an executable format (ex: a Docker image)
4. **create** the hosting infrastructure
5. **deploy** the code into a hosting environment
6. perform all required **acceptance tests** on the deployed application
    * functional testing (using an automated browser or a tool to test the APIs)
    * performance testing
    * Dynamic Application Security Testing ([DAST](https://en.wikipedia.org/wiki/Dynamic_application_security_testing))
    * ...
7. **publish** the validated code and/or package somewhere
8. and lastly **deploy to production**

!!! IMPORTANT
    _to be continuous_ provides predefined, configurable and extensible templates covering one or several of the above stages.

## Several kinds of template

_to be continuous_ provides 6 kinds of template, each one related to a specific part of your pipeline.

### Build & Test

Build & Test templates depend on the language/build system and are in charge of:

* building and unit testing the code,
* providing all _language specific_ code analysis tools (linters, [SAST](https://en.wikipedia.org/wiki/Static_program_analysis), dependency check, ...),
* publishing the built artifacts on a package repository.

### Code Analysis

Code Analysis templates provide code analysis tools ([SAST](https://en.wikipedia.org/wiki/Static_program_analysis),
dependency check, ...) not dependent on any specific language or build tool (ex: SonarQube, Checkmarx, Coverity).

### Packaging

Packaging templates provide tools allowing to package the code into a specific executable/distributable package (ex: Docker, YUM, DEB, ...).
They also provide security tools related to the packaging technology (linters, dependency checks, ...).

### Infrastructure

Infrastructure(_-as-code_) templates are in charge of managing and provisioning your infrastructure resources (network, compute, storage, ...).

### Deploy & Run

Deploy & Run templates depend on the hosting (cloud) environment and are in charge of deploying the code to the hosting
environment.

### Acceptance

Acceptance templates provide acceptance test tools (functional testing, performance testing, [DAST](https://en.wikipedia.org/wiki/Dynamic_application_security_testing)).

## Deployment environments

All our Deploy & Run templates support 4 kinds of environments (each being optional):

| Environment Type | Description                                       | Associated branch(es)                  |
| ---------------- | ------------------------------------------------- | -------------------------------------- |
| **Review**       | Those are dynamic and ephemeral environments to deploy your ongoing developments.<br/> It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature. | All **development branches** (non-integration, non-production) |
| **Integration**  | A single environment to continuously deploy your integration branch. | The **integration branch** (`develop` by default) |
| **Staging**      | A single environment to continuously deploy your production branch.<br/> It is an iso-prod environment, meant for running the automated acceptance tests prior to deploying to the production env. | The **production branch** (`master` or `main`  by default) |
| **Production**   | _Well.. the prod!_ | The **production branch** (`main` or `master`  by default) |

A few remarks:

* All our Acceptance templates support those environments and cooperate gracefully with whichever deployment technology you're using to test the right
  server depending on the branch it's running on.
* Transition from **Staging** to **Production** can be either automatic (if you feel confident enough with your automated acceptance tests) or _one-click_ (this is the default). This is configurable.
* If you're working in an organization where development and deployment are managed by separate teams, you may perfectly not declare any **Production** 
  environment in the development project, but instead trigger a pipeline in the project owned by the deployment team.
* [More info about deployment environments on Wikipedia](https://en.wikipedia.org/wiki/Deployment_environment).

## Generic pipeline stages

Our GitLab templates keep using a coherent set of generic GitLab CI [stages](https://docs.gitlab.com/ee/ci/yaml/#stages),
mapped on the generic pipeline depicted in the previous chapter:

| Stage            | Template type                                     | Description                            |
| ---------------- | ------------------------------------------------- | -------------------------------------- |
| `build`          | <span style="color:#739dcb">build & test</span>   | Build (_when applicable_), unit test (_with code coverage_), and package the code |
| `test`           | <span style="color:#739dcb">build & test</span> / <span style="color:#aaa">code analysis</span> | Perform code anaysis jobs (code quality, Static Application Security Testing, dependency check, license check, ...) |
| `package-build`  | <span style="color:#ec9ba4">packaging</span>      | Build the deployable package |
| `package-test`   | <span style="color:#ec9ba4">packaging</span>      | Perform all tests on package |
| `infra`          | <span style="color:#673ab7">infrastructure</span> | Instantiate/update the (non-production) infrastructure |
| `deploy`         | <span style="color:#79bb6b">deploy & run</span>   | Deploy the application to a (non-production) environment |
| `acceptance`     | <span style="color:#aaaaaa">acceptance</span>     | Perform acceptance tests on the upstream environment |
| `publish`        | <span style="color:#739dcb">build & test</span>   | Publish the packaged code to an artifact repository |
| `infra-prod`     | <span style="color:#673ab7">infrastructure</span> | Instantiate/update the production infrastructure |
| `production`     | <span style="color:#79bb6b">deploy & run</span>   | Deploy the application to the [production](https://en.wikipedia.org/wiki/Deployment_environment#Production) environment (CD pipeline only) |

!!! warning "Ambiguous naming?"

    * `build` stage is not only related to **building** the code, but also running **unit tests** (with code coverage)
    * `test` is not related to unit testing, but more **code analysis**.
    
    We chose to keep those names anyway to stay compatible with GitLab Auto DevOps that has the same philosophy.

Your `.gitlab-ci.yml` file will have to declare **all stages required by included templates**, in the right order.

!!! TIP

    Instead of keeping track of required stages, simply **add them all** in your `.gitlab-ci.yml`:
    
    ```yaml
    stages:
      - build
      - test
      - package-build
      - package-test
      - infra
      - deploy
      - acceptance
      - publish
      - infra-prod
      - production
    ```
    
    :bulb: you may think the complete list is too large for you case, but don't worry: each stage only appears if at 
    least one active job is mapped to it. Therefore - for e.g. - if you're not using any packaging template, the 
    `package-xxx` stages will never show up in your pipelines.

## Git workflows

So far, we've presented a quite _static_ vision of what a CI/CD pipeline should be, but the reality is somewhat different
depending whether it's triggered on a **development branch** (that's CI) or on the **production branch** (that's CD).

### The guiding principles

!!! Success "The guiding principles"

    * continuous integration (CI) has to be **fast** (and to some extend _energy efficient_)
    * continuous deployment/delivery (CD) has to **secure** the deployment/delivery to production

**Continuous integration** is what a developer keeps doing 90% of the day: change a bit of code, commit, push and wait
for the code to be up and running somewhere.
Such a cycle may occur up to 30 times per day, sometimes more.
That's why it has to be **as fast as possible**, as every saved minute is a huge gain for the developer's productivity at
the end of the day.
That's also why it may not be wise to start a complete non-regression automated test campaign every time the developer 
changes a comma in his code.

On the contrary, **continuous deployment/delivery** occurs much less often but denotes the will of rolling out a new version
of your code.
That's why it has to make everything possible to **maximize the confidence** in what you are doing.

### How is it mapped on Git workflows?

Using Git, there are [many possible workflows](https://www.atlassian.com/git/tutorials/comparing-workflows).

You are free to use whichever you want, but our templates make strong hypothesis you should be aware of:

* the `master` (or `main`) branch is **production** (triggers the CD pipeline),
* the `develop` branch is **integration** (triggers an hybrid pipeline),<br/>
  :warning: _the use of an integration branch is **optional**, and even 
  [discouraged in the general case](#when-to-use-an-integration-branch)_
* any other branch is **development** (triggers the CI pipeline).

The following schemas summarize the 2 main branching strategies (depending on whether you will be using an 
integration branch or not) with their associated deployment strategy:

=== "Without `develop`"

    ![Feature-Branch strategy](img/Feature-Branch-strategy.drawio.svg)

=== "With `develop`"

    ![Gitflow-like branching strategy](img/Gitflow-like-strategy.drawio.svg)

    > :bulb: If you're using a complete [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) workflow,
    > _release_ branches are managed the same way as regular _development_ branches, possibly deploying to dedicated review environments.

Now, here is a table that summarizes the generic templates behavior for each branch:

<table border="1">
<thead>
<tr>
<th>Stage/<br>Branch</th>
<th style="vertical-align: middle">build</th>
<th style="vertical-align: middle">test</th>
<th style="vertical-align: middle">infra/deploy</th>
<th style="vertical-align: middle">acceptance</th>
<th style="vertical-align: middle">infra-prod/production</th>
</tr>
</thead>
<tbody>
<tr style="background-color: #0074D940;">
<td><strong>development</strong> (any branch other than <code>develop</code>, <code>main</code> or <code>master</code>)</td>
<td rowspan=3 style="vertical-align: middle; background-color: #ddd;">compile &amp; unit test</td>
<td><strong>quick analysis</strong> <i>(when possible)</i>, or <strong>manual</strong> trigger</td>
<td>create/deploy a <code>review</code> environment</td>
<td><strong>manual</strong> trigger</td>
<td>N/A</td>
</tr>
<tr style="background-color: #FF851B40;">
<td><strong>integration</strong> (<code>develop</code>&nbsp;branch)</td>
<td rowspan=2 style="vertical-align: middle; background-color: #eee;"><strong>deep analysis</strong> / <strong>automatic</strong> trigger</td>
<td>create/deploy the <code>integration</code> environment</td>
<td rowspan=2 style="vertical-align: middle; background-color: #eee;"><strong>automatic</strong> trigger</td>
<td>N/A</td>
</tr>
<tr style="background-color: #FF413640;">
<td><strong>production</strong> (<code>main</code>&nbsp;or&nbsp;<code>master</code>&nbsp;branch)</td>
<td>create/deploy the <code>staging</code> environment</td>
<td>create/deploy the <code>production</code> environment</td>
</tr>
</tbody>
</table>

### When to use an integration branch?

Let's state it clearly: **the most efficient Git workflow is the simplest one** that fits your needs.
Consequently:

1. If you do not have good reasons of using an integration branch, just don't.
2. If you do not know which Git workflow to use, start as simple as possible.
3. Feature-Branch shall be enough in most situations.

??? ATTENTION "What is the harm with an integration branch?"
    Using an integration branch has several drawbacks that you shall be aware of before making your choice.

    The root cause is it introduces a de-facto delay between the end of a development (feature branch being merged into `develop`) and its deployment to the production environment (`develop` being merged into `main`, thus flushing accumulated changes all at once).

    This "two-stages" deployment raises issues:

    * Who is responsible of flushing `develop` into `main`? When?
    * When things go wrong during a deployment to production, it might be complex to identify which change caused the issue (there might even be cases where the problem is actually due to the interaction between 2 separate changes).
    * Depending on the time elapsed since the end of development, it may be difficult for the author of the failing code to analyze the reasons of the problem if this development dates back to a few weeks or months, and the developer has moved on to other tasks.

Using an integration branch is discouraged in the general case, but there are some acceptable reasons to adopt one. 
The following chapters present the 3 main ones.

#### Can't afford Review environments

Instantiating a dedicated hosting environment for each development branch in progress might be indeed a cost difficult to afford.

!!! IMPORTANT

    Keep in mind there might be tricks to mitigate this cost: 

    * shutdown all review environments every evening, 
    * use a degraded infrastructure (ex: use an in-memory database instead of a real one, get rid of all redundancy, …)

Even with smart ideas, it may occur that review environments are just not affordable.
In that case your developers will need a single, shared environment to integrate all their work.
That's exactly the purpose of an integration branch.

#### Not mature enough for continuous deployment

Lack of automated testing, need of manual acceptance tests campaign on a dedicated environment, poor software quality… 
There are many reasons why you may not feel ready for continuous deployment.

In that case, an integration branch with its associated integration environment might be the adapted solution.

#### Release-oriented deployment

In release-oriented projects, several features get bundled into a release and then deployed all at once.
With the release often comes a versioning strategy, release notes, roadmap, something very common in software edition business.

In that case, an integration branch would be the most appropriate way of addressing this requirement.

Thus developers will develop changes and continuously integrate them into the integration branch, and once all expected features
have been developed, you'll be able to proceed with the release and _flush_ them all by merging `develop` into `main`.

## Modularity & Composability

_to be continuous_ templates are built to be:

* **Modular**: each template complies to the [Single-responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) while observing common architectural principles (standard behaviour per template type, generic pipeline stages, common Git workflow principles, ...)
* **Composable**: each template cooperates gracefully with others to minimize the amount of integration work. 

Let's illustrate this with an example.
Here is a pipeline of a project using multiple templates:

* Maven to build and test the Java code,
* SonarQube to analyse the code,
* Docker to containerize the application,
* Kubernetes to deploy and run the containers,
* Cypress and Postman for automated acceptance tests.

![Modular Pipeline Example](img/example-pipeline-1.png)

Composability highlights:

* the Docker build uses the artifacts produced by Maven to build the container,
* the SonarQube analysis reuses the unit tests and coverage report from Maven,
* Kubernetes deploys the Docker container built in the upstream jobs,
* Cypress and Postman automatically test the application deployed by Kubernetes in the upstream pipeline.

Other examples of composability in _to be continuous_:

* Most templates implementing publish/release feature also cooperate with the Semantic Release
  template (when used) to retrieve the next version number from Semantic Release.
* the Terraform template supports techniques to propagate generated inventory information (easy to reuse with Ansible or AWS template for instance).
