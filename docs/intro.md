---
author: Pierre Smeyers
description: to-be-continuous is a set of GitLab CI templates developed and maintained by DevOps and technology experts to build state-of-the-art CI/CD pipelines in minutes.
---

# To Be Continuous

_to be continuous_ proposes a set of GitLab CI templates developed and maintained by DevOps and technology experts to build state-of-the-art CI/CD pipelines in minutes.

## Key features

| Feature             | Description |
| ------------------- | ----------- |
| Easy                | No need to master GitLab CI: follow the guide and include the templates you need. |
| Modular             | Build your project pipeline by assembling every required template. |
| Security            | Ease the use of security & quality tools (code quality, SAST, dependency check, license management, DAST, ...). |
| Git Workflows       | Our templates support modern DevOps workflows ([Feature Branch](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow), [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow), ...). |
| Up-To-Date          | We carefully follow every GitLab CE release to integrate every new [feature](https://about.gitlab.com/features/) to our templates. |
| Configurable & Extensible | Comply to the convention over configuration principle. <br> Work with minimal configuration, yet can be configured to fit specific needs. |
| Review Environments | Dynamically created environments in your cloud, hosting your development in progress. <br>It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature. |
| Documented          | All our templates have an extensive online documentation. |
| Examples            | We also provide a wide variety of project samples to illustrate their use and best practices. |

## Implemented GitLab features

| Feature             | Description |
| ------------------- | ----------- |
| [cache](https://docs.gitlab.com/ee/ci/caching/) | Every template carefully implements optimized cache policy to speedup your pipeline execution. |
| [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) | Every template implements optimized artifacts policy to keep only the bare necessities. |
| [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) | Every interruptible job implements the `interruptible` feature to allow cancelling the pipeline as soon as a newer one is triggered. |
| [code coverage](https://docs.gitlab.com/ee/ci/yaml/#coverage) | Every build&test job enforces code coverage computing (using the required build tools and options), and integration to GitLab (visible in merge requests and as a [badge](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-report-badge)). |
| [JUnit test reports](https://docs.gitlab.com/ee/ci/junit_test_reports.html) | Every test job produces - whenever possible - a JUnit report, so that your test reports are automatically integrated to your pipelines and merge requests. |
| [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) | Every code quality job produces - whenever possible - a Code Climate report, so that your code quality reports are automatically integrated to your pipelines and merge requests. |
| [environments](https://docs.gitlab.com/ee/ci/environments/) | Every deployment job declares the deployed environment to GitLab. They also implement a cleanup job for ephemeral environments, that can be triggered manually and also automatically on branch deletion. |
| [resource_group](https://docs.gitlab.com/ee/ci/yaml/#resource_group) | Every deployment job implements the `resource_group` feature to prevent concurrent jobs deploying at the same time on the same environment. |

## How does it work?

_to be continuous_ templates rely on the GitLab CI [`include:file` feature](https://docs.gitlab.com/ee/ci/yaml/#includefile).

Any template may be included in your `.gitlab-ci.yml` file as (for e.g.):

```yaml
include:
  - project: 'to-be-continuous/maven' # this is the template project
    file: '/templates/gitlab-ci-maven.yml' # template file within the project
    ref: '1.0.0' # template version
```
