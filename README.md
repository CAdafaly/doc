# To Be Continuous website

This project build and exposes _to be continuous_ website as [GitLab pages](to-be-continuous.gitlab.io/doc).

The documentation is built with [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

It also generates the assembled Kicker descriptor (one single JSON file containing all Kicker resources).

## Build and test locally

1. Install [MkDocs](https://www.mkdocs.org/#installation) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/),
2. Run with `mkdocs server`,
3. Open [http://127.0.0.1:8000](http://127.0.0.1:8000) in your browser.

In this mode, MkDocs updates any change on-the-fly.

## How to configure the assembled Kicker descriptor build ?

The assembled Kicker descriptor is built by crawling one or several GitLab groups/projects, looking for Kicker descriptors.

It can be configured with the following variables:

* `GITLAB_TOKEN`: a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with scopes `api,read_repository`
  and at least `Developer` role on all groups & projects to crawl (not required if only `public` groups and projects),
* `PREF_TAG_PATTERN`: preferred tag pattern.
* `KICKER_RESOURCE_GROUPS`: JSON configuration of GitLab groups to crawl.

### Details about `PREF_TAG_PATTERN`

The algorithm to determine which template latest version to include in Kicker is the following:

1. get all published (Git) tags, 
2. sort them (using [Version sort](https://www.gnu.org/software/coreutils/manual/html_node/Version-sort-overview.html)),
3. among this list, filter all tags matching a _preferred pattern_ (regex)
    * one or more matches: return the latest one from the filtered list
    * none matches (fallback): simply pick the latest one from the unfiltered list

The `PREF_TAG_PATTERN` variable allows to define the preferred version pattern to be used as the latest version to include in Kicker.

By default it is set as `^v?[0-9]+\.[0-9]+$`. In other words, Kicker will propose the latest published 
minor version alias (ex `2.4`), when present.

If your want another behavior in your self-managed GitLab, simply override this variable in your local copy 
of the `to-be-continuous/doc` project. Example values:

* full semver version (incuding fix part): `^v?[0-9]+\.[0-9]+\.[0-9]+$`
* simply the latest tag (whichever the format): `.*`

### Details about `KICKER_RESOURCE_GROUPS`

Here is an example of `KICKER_RESOURCE_GROUPS` content:

```json
[
  {
    "path": "acme/cicd/all", 
    "visibility": "public"
  },
  {
    "path": "acme/cicd/ai-ml", 
    "visibility": "internal", 
    "exclude": ["project-2", "project-13"],
    "extension": 
      {
        "id": "ai-ml", 
        "name": "AI/ML", 
        "description": "ACME templates for AI/ML projects"
      }
  },
  {
    "path": "to-be-continuous", 
    "visibility": "public"
  }
]
```

Some explanations:

* `path` is a path to a [GitLab group](https://docs.gitlab.com/ee/user/group/)
  with [GitLab projects](https://docs.gitlab.com/ee/user/project/) containing Kicker resources.
* `visibility` is the group/projects visibility to crawl.
* `exclude` (optional) allows to exclude some project(s) from processing.
* `extension` (optional) allows to associate Kicker resources with a separate extension (actionable within Kicker).

By default, `KICKER_RESOURCE_GROUPS` is configured to crawl the `to-be-continuous` group only.

## Tracking script configuration

Another thing that can be configured is how you will track audience on the _to be continuous_ website.

All you have to do is to defined a `TRACKING_JS` variable with tracking JavaScript code ([Google Analytics](https://analytics.google.com/), [Matomo](https://matomo.org/) or else).

Example of `TRACKING_JS` value for Google Analytics:

```javascript
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-123456789-1', 'auto');
ga('send', 'pageview');
```
