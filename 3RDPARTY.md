# Third Party Software

This project uses 3rd party software:

* [Orange Boosted with Bootstrap](https://github.com/Orange-OpenSource/Orange-Boosted-Bootstrap)
  distributed under the terms of the [MIT License](https://github.com/Orange-OpenSource/Orange-Boosted-Bootstrap/blob/v4-dev/LICENSE)
* [Font Awesome](https://github.com/FortAwesome/Font-Awesome)
  distributed under the terms of the [Font Awesome Free License](https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
* [jQuery](https://github.com/jquery/jquery)
  distributed under the terms of the [MIT License](https://github.com/jquery/jquery/blob/main/LICENSE.txt)
